package com.xiyuan.client.controller;

import com.xiyuan.client.config.NettyClientUtil;
import com.xiyuan.common.annotation.MethodLogPrint;
import com.xiyuan.common.model.ResponseResult;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.util.FileUtil;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.IOException;

@RestController
@Slf4j
public class UserController {

    @GetMapping("/helloNetty")
    @MethodLogPrint
    public ResponseResult helloNetty() throws IOException {
        String msg = FileUtil.readAsString(new File("E:\\back\\WPFSData\\XNJTFDC_DQ_20210813_0000.WPD"));

        return NettyClientUtil.helloNetty(msg);
    }

}
